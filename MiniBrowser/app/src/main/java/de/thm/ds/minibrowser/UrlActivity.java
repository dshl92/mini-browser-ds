package de.thm.ds.minibrowser;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;

public class UrlActivity extends Activity {

    public static final String EXTRA_URL = "EXTRA_URL";
    private static final String TAG = UrlActivity.class.getName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.url_layout);

        // log orientation
        int orientation = this.getResources().getConfiguration().orientation;
        String orientationString;
        switch (orientation)
        {
            case Configuration.ORIENTATION_LANDSCAPE:
                orientationString = "Landscape";
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                orientationString = "Portrait";
                break;
            case Configuration.ORIENTATION_UNDEFINED:
                orientationString = "Undefined";
                break;
            default:
                orientationString = String.valueOf(orientation);
                break;
        }

        Log.d(TAG, "current orientation: " + orientationString);
    }

    public void openWebsite(View view) {
        EditText urlEditText = this.findViewById(R.id.urlEditText);
        String url = urlEditText.getText().toString();

        Log.i(TAG, "Open Website: " + url);

        Intent intent = new Intent(this, ViewActivity.class);
        intent.putExtra(EXTRA_URL, url);
        this.startActivity(intent);
    }

}
